# MultipleTheme

#### 项目介绍
- 项目名称：MultipleTheme
- 所属系列：openharmony的第三方组件适配移植
- 功能：支持无缝换肤的框架，配合theme和换肤控件框架可以做到无缝切换换肤
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：master分支
#### 效果演示
![](multipleTheme.gif)
#### 安装教程

1.下载app的hap包multipleTheme.hap（位于:https://gitee.com/chinasoft2_ohos/MultipleTheme/tree/1.0.0 ）

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
###
第一步：在项目的pattern.json声明自定义属性（各种模式都会用到的属性）
###
Setup 1:declare attribute in pattern.json
```
"pattern": [
    {
      "name": "base",
      "value": [
        {
          "name": "width",
          "value": "100vp"
        },
        {
          "name": "background_element",
          "value": "$graphic:background_ability_main"
        },
        {
          "name": "text_color",
          "value": "#ffff0000"
        },
        {
          "name": "text_size",
          "value": "200fp"
        }
      ]
    }
  ]
```
###
第二步：在项目的theme.json指定各种模式主题下的自定义属性值
###
Setup 2:declare attribute in theme.json
```
"theme": [
    {
      "name": "theme1",
      "value": [
        {
          "name": "main_background",
          "value": "$graphic:background_ability_main"
        },
        {
          "name": "main_text_color",
          "value": "$color:color_1"
        },
        {
          "name": "second_background",
          "value": "#0000ff"
        },
        {
          "name": "second_text_color",
          "value": "#00ff00"
        }
      ]
    }
```
###
第三步：在页面布局文件里使用自定义属性值
###
Setup 3:use attribute in layout
```
  <derson.com.multipletheme.colorUi.widget.ColorTextView
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:text="$string:HelloWorld"
        ohos:text_size="14fp"
        ohos:text_color="#000"
        ohos:theme_prefix="main"/>
```
###
第四步：在基类的onCreate方法里添加切换主题模式的逻辑代码
###
Setup 4:add the code of switch theme－mode in base ability
###
```
@Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(getLayoutId());

        if(SharedPreferencesMgr.getInt("theme", 0) == 1) {
            setTheme(ResourceTable.Theme_theme2);
        } else {
            setTheme(ResourceTable.Theme_theme1);
        }
        ColorUiUtil.changeTheme(getRootView(), getTheme());
```
###
第五步：调用工具类方法切换主题模式
###
Setup 5:switch theme-mode in code
```
ColorUiUtil.changeTheme(getRootView(), getTheme());
###
```
###
第六步：针对切换主题模式时需要立即更新页面ui的页面，需要使用框架里的封装控件
###
Setup 6:use customize-component of framework at the ability that need update ui when switch theme-mode. 
###
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT



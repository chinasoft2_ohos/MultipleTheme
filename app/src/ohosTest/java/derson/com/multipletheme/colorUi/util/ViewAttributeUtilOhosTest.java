/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain an copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package derson.com.multipletheme.colorUi.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * ViewAttributeUtil单元测试
 */
public class ViewAttributeUtilOhosTest {
    /**
     * 测试buildParamNotNull
     */
    @Test
    public void buildParamNotNull() {
        String param = "param";
        String attr = "attr";
        String result = ViewAttributeUtil.buildParam(param, attr);
        Assert.assertNotNull(result);
        Assert.assertEquals(attr+"_"+param,result);
    }

    /**
     * 测试buildParamAttrIsNull
     */
    @Test
    public void buildParamAttrIsNull() {
        String param = "param";
        String attr = "";
        String result = ViewAttributeUtil.buildParam(param, attr);
        Assert.assertNotNull(result);
        Assert.assertEquals(param,param);
    }
}
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain an copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package derson.com.multipletheme.colorUi.util;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * SharedPreferencesMgr单元测试
 */
public class SharedPreferencesMgrOhosTest {
    private Context context;
    private SharedPreferencesMgr sharedPreferencesMgr;

    /**
     * 前置，关键对象初始化
     */
    @Before
    public void setUp(){
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        sharedPreferencesMgr = SharedPreferencesMgr.getInstance(context);
        sharedPreferencesMgr.setInt("testGetInt",1);
        sharedPreferencesMgr.setBoolean("testGetBoolean",true);
        sharedPreferencesMgr.setString("testGetString","test");
        Assert.assertNotNull(context);
    }
    /**
     * 测试getInstance
     */
    @Test
    public void getInstance() {
        Assert.assertNotNull(sharedPreferencesMgr);
    }
    /**
     * 测试getInt
     */
    @Test
    public void getInt() {
        Assert.assertEquals(1,sharedPreferencesMgr.getInt("testGetInt",-1));
    }

    /**
     * 测试setInt
     */
    @Test
    public void setInt() {
        int data = 2;
        sharedPreferencesMgr.setInt("setInt",data);
        int result = sharedPreferencesMgr.getInt("setInt",-1);
        Assert.assertEquals(data,result);
    }

    /**
     * 测试getBoolean
     */
    @Test
    public void getBoolean() {
        Assert.assertEquals(true,sharedPreferencesMgr.getBoolean("testGetBoolean",false));
    }

    /**
     * 测试setBoolean
     */
    @Test
    public void setBoolean() {
        boolean data = true;
        sharedPreferencesMgr.setBoolean("setBoolean",data);
        boolean result = sharedPreferencesMgr.getBoolean("setBoolean",false);
        Assert.assertEquals(data,result);
    }

    /**
     * 测试getString
     */
    @Test
    public void getString() {
        String testGetString= sharedPreferencesMgr.getString("testGetString",null);
        Assert.assertNotNull(testGetString);
        Assert.assertEquals("test",testGetString);
    }
    /**
     * 测试setString
     */
    @Test
    public void setString() {
        String data = "data";
        sharedPreferencesMgr.setString("setString",data);
        String result = sharedPreferencesMgr.getString("setString",null);
        Assert.assertNotNull(result);
        Assert.assertEquals(data,result);
    }

    /**
     * 测试clearAll
     */
    @Test
    public void clearAll() {
        String data = sharedPreferencesMgr.getString("testGetString",null);
        Assert.assertNotNull(data);
        sharedPreferencesMgr.clearAll();
        String data2 = sharedPreferencesMgr.getString("testGetString",null);
        Assert.assertNull(data2);
    }
}
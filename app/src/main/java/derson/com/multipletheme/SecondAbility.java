package derson.com.multipletheme;

import ohos.aafwk.content.Intent;

/**
 * SecondAbility
 *
 * @author Administrator
 * @Date 2021/5/26
 */
public class SecondAbility extends BaseAbility {
    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_ability_second;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

}

package derson.com.multipletheme;

import derson.com.multipletheme.colorUi.util.SharedPreferencesMgr;
import ohos.aafwk.ability.AbilityPackage;

/**
 * MyApplication
 *
 * @author Administrator
 * @Date 2021/5/26
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        SharedPreferencesMgr.getInstance(this);
    }
}

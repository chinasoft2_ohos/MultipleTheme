
package derson.com.multipletheme;

import derson.com.multipletheme.colorUi.util.ColorUiUtil;
import derson.com.multipletheme.colorUi.util.SharedPreferencesMgr;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

/**
 * BaseAbility
 *
 * @author Administrator
 * @Date 2021/5/26
 */
public abstract class BaseAbility extends Ability {
    /**
     * 资源文件id
     *
     * @return LayoutId
     */
    protected abstract int getLayoutId();

    /**
     * Component
     *
     * @return Component
     */
    protected Component getRootView() {
        return findComponentById(ResourceTable.Id_root);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(getLayoutId());
        if (SharedPreferencesMgr.getInstance(this).getInt("theme", 0) == 1) {
            setTheme(ResourceTable.Theme_theme2);
        } else {
            setTheme(ResourceTable.Theme_theme1);
        }
        ColorUiUtil.changeTheme(getRootView(), getTheme());
    }
}


package derson.com.multipletheme;

import derson.com.multipletheme.colorUi.util.ColorUiUtil;
import derson.com.multipletheme.colorUi.util.SharedPreferencesMgr;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;

/**
 * MainAbility
 *
 * @author Administrator
 * @Date 2021/5/26
 */
public class MainAbility extends BaseAbility {
    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_ability_main;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        Button button = (Button) findComponentById(ResourceTable.Id_btn);
        button.setClickedListener(component -> {
            if (SharedPreferencesMgr.getInstance(this).getInt("theme", 0) == 1) {
                SharedPreferencesMgr.getInstance(this).setInt("theme", 0);
                setTheme(ResourceTable.Theme_theme1);
            } else {
                SharedPreferencesMgr.getInstance(this).setInt("theme", 1);
                setTheme(ResourceTable.Theme_theme2);
            }
            ColorUiUtil.changeTheme(getRootView(), getTheme());
        });
        Button next = (Button) findComponentById(ResourceTable.Id_next);
        next.setClickedListener(component -> {
            Intent secondIntent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withBundleName("derson.com.multipletheme")
                    .withAbilityName("derson.com.multipletheme.SecondAbility")
                    .build();
            secondIntent.setOperation(operation);
            startAbility(secondIntent);
        });
    }

}

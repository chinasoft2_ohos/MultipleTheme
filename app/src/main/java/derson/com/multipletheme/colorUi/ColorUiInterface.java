package derson.com.multipletheme.colorUi;

import ohos.agp.components.Component;
import ohos.global.resource.solidxml.Theme;

/**
 * 换肤接口
 * Created by chengli on 15/6/8.
 */
public interface ColorUiInterface {
    Component getView();

    void setTheme(Theme themeId);
}

package derson.com.multipletheme.colorUi.widget;

import derson.com.multipletheme.colorUi.ColorUiInterface;
import derson.com.multipletheme.colorUi.util.ViewAttributeUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;
import ohos.global.resource.solidxml.Theme;

/**
 * Created by chengli on 15/6/11.
 */
public class ColorDirectionalLayout extends DirectionalLayout implements ColorUiInterface {
    private String attr = "";

    public ColorDirectionalLayout(Context context) {
        super(context);
    }

    public ColorDirectionalLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    public ColorDirectionalLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    @Override
    public Component getView() {
        return this;
    }

    @Override
    public void setTheme(Theme themeId) {
        ViewAttributeUtil.applyBackgroundDrawable(this, themeId, attr);
    }
}

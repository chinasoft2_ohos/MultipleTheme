package derson.com.multipletheme.colorUi.util;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

/**
 * SharedPreferences管理类
 */
public class SharedPreferencesMgr {
    private static SharedPreferencesMgr sharedPreferencesUtils;
    /**
     * 保存在手机里面的文件名
     */
    private String FILE_NAME = "derson";
    private Preferences sPrefs;

    /**
     * 初始化
     *
     * @param context 上下文
     * @return 本地SP对象
     */
    public static SharedPreferencesMgr getInstance(Context context) {
        if (sharedPreferencesUtils == null) {
            sharedPreferencesUtils = new SharedPreferencesMgr(context);
        }
        return sharedPreferencesUtils;
    }

    private SharedPreferencesMgr(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        sPrefs = databaseHelper.getPreferences(FILE_NAME);
    }

    /**
     * 通过KEY查找
     *
     * @param key          key值
     * @param defaultValue 默认值
     * @return 显示ID
     */
    public int getInt(String key, int defaultValue) {
        return sPrefs.getInt(key, defaultValue);
    }

    /**
     * 通过KEY查找
     *
     * @param key   key值
     * @param value 默认值
     */
    public void setInt(String key, int value) {
        sPrefs.putInt(key, value).flushSync();
    }

    /**
     * 通过KEY查找
     *
     * @param key          key
     * @param defaultValue defaultValue
     * @return 是true否false
     */
    public boolean getBoolean(String key, boolean defaultValue) {
        return sPrefs.getBoolean(key, defaultValue);
    }

    /**
     * 内容
     *
     * @param key   key
     * @param value value
     */
    public void setBoolean(String key, boolean value) {
        sPrefs.putBoolean(key, value).flushSync();
    }

    /**
     * 内容
     *
     * @param key          key
     * @param defaultValue defaultValue
     * @return 显示内容
     */
    public String getString(String key, String defaultValue) {
        if (sPrefs == null) {
            return defaultValue;
        }
        return sPrefs.getString(key, defaultValue);
    }

    /**
     * 内容
     *
     * @param key   key
     * @param value value
     */
    public void setString(String key, String value) {
        if (sPrefs == null) {
            return;
        }
        sPrefs.putString(key, value).flushSync();
    }

    /**
     * 清除
     */
    public void clearAll() {
        if (sPrefs == null) {
            return;
        }
        sPrefs.clear().flushSync();
    }
}

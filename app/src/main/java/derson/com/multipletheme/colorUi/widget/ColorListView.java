package derson.com.multipletheme.colorUi.widget;

import derson.com.multipletheme.colorUi.ColorUiInterface;
import derson.com.multipletheme.colorUi.ResourceName;
import derson.com.multipletheme.colorUi.util.ViewAttributeUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.solidxml.Theme;
import ohos.global.resource.solidxml.TypedAttribute;

import java.io.IOException;

import static derson.com.multipletheme.colorUi.util.ViewAttributeUtil.buildParam;

/**
 * Created by chengli on 15/6/8.
 */
public class ColorListView extends ListContainer implements ColorUiInterface {
    private String attr = "";

    public ColorListView(Context context) {
        super(context);
    }

    public ColorListView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    public ColorListView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    @Override
    public Component getView() {
        return this;
    }

    @Override
    public void setTheme(Theme theme) {
        ViewAttributeUtil.applyBackgroundDrawable(this, theme, attr);
        String key = buildParam(ResourceName.DIVIDER, attr);
        if (theme.getThemeHash().get(key) == null) {
            return;
        }
        try {
            TypedAttribute ta = theme.getValue(key);
            String origin = ta.getParsedValue();
            if (origin.contains("/graphic/")) {
                this.setBoundary(
                        new ShapeElement(getContext(), ta.getResId()));
            } else if (ta.getType() == TypedAttribute.UNDEFINED_ATTR && origin.startsWith("#")) {
                this.setBoundaryColor(new Color(Color.getIntColor(origin)));
            } else {
                return;
            }
        } catch (NotExistException | WrongTypeException | IOException e) {
            e.printStackTrace();
        }
    }
}

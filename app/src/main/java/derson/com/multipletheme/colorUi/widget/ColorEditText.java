package derson.com.multipletheme.colorUi.widget;

import derson.com.multipletheme.colorUi.ColorUiInterface;
import derson.com.multipletheme.colorUi.util.ViewAttributeUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.app.Context;
import ohos.global.resource.solidxml.Theme;

/**
 * Created by chengli on 15/6/8.
 */
public class ColorEditText extends TextField implements ColorUiInterface {
    private String attr = "";

    public ColorEditText(Context context) {
        super(context);
    }

    public ColorEditText(Context context, AttrSet attrSet) {
        super(context, attrSet);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    public ColorEditText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        attr = ViewAttributeUtil.getAttributeValue(attrSet);
    }

    @Override
    public Component getView() {
        return this;
    }

    @Override
    public void setTheme(Theme themeId) {
        ViewAttributeUtil.applyBackgroundDrawable(this, themeId, attr);
        ViewAttributeUtil.applyTextColor(this, themeId, attr);
        ViewAttributeUtil.applyTextSize(this, themeId, attr);
    }
}

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain an copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package derson.com.multipletheme.colorUi;

/**
 * ResourceName
 *
 * @author Administrator
 * @Date 2021/5/26
 */
public class ResourceName {
    /**
     * theme_prefix
     */
    public static final String ATTR_NAME = "theme_prefix";
    /**
     * background
     */
    public static final String BACKGROUND = "background";
    /**
     *
     */
    public static final String IMAGE_SRC = "image_src";
    /**
     * image_src
     */
    public static final String TEXT_COLOR = "text_color";
    /**
     * text_size
     */
    public static final String TEXT_SIZE = "text_size";
    /**
     * divider
     */
    public static final String DIVIDER = "divider";
}

package derson.com.multipletheme.colorUi.util;

import derson.com.multipletheme.colorUi.ColorUiInterface;
import derson.com.multipletheme.colorUi.ResourceName;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.solidxml.Theme;
import ohos.global.resource.solidxml.TypedAttribute;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by chengli on 15/6/8.
 */
public class ViewAttributeUtil {
    /**
     * AttrSet
     *
     * @param attr AttrSet
     * @return 内容
     */
    public static String getAttributeValue(AttrSet attr) {
        Optional<Attr> op = attr.getAttr(ResourceName.ATTR_NAME);
        if (op.isPresent()) {
            return op.get().getStringValue();
        }
        return "";
    }

    /**
     * param
     *
     * @param param buildParam
     * @param attr  AttrSet
     * @return 内容
     */
    public static String buildParam(String param, String attr) {
        return attr.length() <= 0 ? param : (attr + "_" + param);
    }

    /**
     * 设置背景
     *
     * @param ci    ColorUiInterface
     * @param theme 主题
     * @param attr  AttrSet
     */
    public static void applyBackgroundDrawable(ColorUiInterface ci, Theme theme, String attr) {
        try {
            String key = buildParam(ResourceName.BACKGROUND, attr);
            if (theme.getThemeHash().get(key) == null) {
                return;
            }
            TypedAttribute ta = theme.getValue(key);
            String origin = ta.getParsedValue();
            if (origin.contains("/graphic/")) {
                (ci.getView()).setBackground(
                        new ShapeElement(ci.getView().getContext(), ta.getResId()));
            } else if (ta.getType() == TypedAttribute.UNDEFINED_ATTR && origin.startsWith("#")) {
                ShapeElement se = new ShapeElement();
                se.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(origin)));
                (ci.getView()).setBackground(se);
            } else {
                return;
            }
        } catch (NotExistException | WrongTypeException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过图片设置
     *
     * @param ci    ColorUiInterface
     * @param theme 主题
     * @param attr  AttrSet
     */
    public static void applyImageDrawable(ColorUiInterface ci, Theme theme, String attr) {
        if (ci instanceof Image) {
            String key = buildParam(ResourceName.IMAGE_SRC, attr);
            if (theme.getThemeHash().get(key) == null) {
                return;
            }
            try {
                TypedAttribute ta = theme.getValue(key);
                String origin = ta.getParsedValue();
                if (origin.contains("/media/")) {
                    ((Image) ci.getView()).setImageElement(new PixelMapElement(ta.getMediaResource()));
                } else if (origin.contains("/graphic/")) {
                    ((Image) ci.getView()).setImageElement(new ShapeElement(
                            ci.getView().getContext(), ta.getResId()));
                } else {
                    return;
                }
            } catch (NotExistException | WrongTypeException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 设置文字颜色
     *
     * @param ci    ColorUiInterface
     * @param theme 主题
     * @param attr  AttrSet
     */
    public static void applyTextColor(ColorUiInterface ci, Theme theme, String attr) {
        if (ci instanceof Text) {
            String key = buildParam(ResourceName.TEXT_COLOR, attr);
            if (theme.getThemeHash().get(key) == null) {
                return;
            }
            try {
                String color = theme.getValue(key).getParsedValue();
                ((Text) ci.getView()).setTextColor(new Color(Color.getIntColor(color)));
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 设置文字大小
     *
     * @param ci    ColorUiInterface
     * @param theme 主题
     * @param attr  AttrSet
     */
    public static void applyTextSize(ColorUiInterface ci, Theme theme, String attr) {
        if (ci instanceof Text) {
            String key = buildParam(ResourceName.TEXT_SIZE, attr);
            if (theme.getThemeHash().get(key) == null) {
                return;
            }
            try {
                float size = theme.getFloatValue(key, 0);
                ((Text) ci.getView()).setTextSize((int) size);
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
        }
    }

}

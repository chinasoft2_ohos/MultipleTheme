package derson.com.multipletheme.colorUi.util;

import derson.com.multipletheme.colorUi.ColorUiInterface;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.global.resource.solidxml.Theme;

/**
 * Created by chengli on 15/6/10.
 */
public class ColorUiUtil {
    /**
     * 切换应用主题
     *
     * @param rootView 视图
     * @param theme    主题
     */
    public static void changeTheme(Component rootView, Theme theme) {
        if (rootView instanceof ColorUiInterface) {
            ((ColorUiInterface) rootView).setTheme(theme);
            if (rootView instanceof ComponentContainer) {
                int count = ((ComponentContainer) rootView).getChildCount();
                for (int i = 0; i < count; i++) {
                    changeTheme(((ComponentContainer) rootView).getComponentAt(i), theme);
                }
            }
        } else {
            if (rootView instanceof ComponentContainer) {
                int count = ((ComponentContainer) rootView).getChildCount();
                for (int i = 0; i < count; i++) {
                    changeTheme(((ComponentContainer) rootView).getComponentAt(i), theme);
                }
            }
        }
    }


}
